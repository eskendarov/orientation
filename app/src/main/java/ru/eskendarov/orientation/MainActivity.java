package ru.eskendarov.orientation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Урок 3. Activity. Смена ориентации экрана. Две Activity
 *
 * @author Enver Eskendarov
 * @date 06/01/19
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

} // MainActivity end
